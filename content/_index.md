---
title: 'Home'
# intro_image: images/undraw_problem_solving_ft81.svg
intro_image: images/back/Logo.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
---

# Cosima Hagyó


Willkommen auf meiner Homepage für systemische Paar- und Familientherapie,
Psychotraumaberatung und traumazentrierte integrative Therapie für Erwachsene und Kinder/Jugendlichen.

Ich begleite und unterstütze Paare, Familien, allein Lebende, Eltern,
sowie Kinder und Jugendliche die mit ihrer gegenwärtigen Situation unzufrieden und/oder überfordert sind und neue Wege finden möchten.
