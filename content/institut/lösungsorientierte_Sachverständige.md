---
title: 'Lösungsorientierte Sachverständige'
date: 2018-11-28T15:14:39+10:00
intro_image: images/undraw_selection_92i4.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
featured: true
draft: false
weight: 2
---

Aus meiner Arbeit als Verfahrensbeistand und Umgangspflegerin kennen sie bereits meinen lösungsorientierten Ansatz. Sofern es die Zeit bis zur gerichtlichen Anhörung erlaubte, entsprach es meinem fachlichen Verständnis auf konstruktive Lösungen hinzuwirken.

Als lösungsorientierte Sachverständige sehe ich meine Aufgabe mit der Familie und eventuell weiteren Beteiligten ein einvernehmliches Konzept zu erarbeiten, welchem im von mir angestrebten Ziel in einer nachhaltigen Vereinbarung münden sollte, die dem Kindeswohl entspricht und sinnvoll für das Kind sein sollte – und im besten Fall auch für die Eltern. (Salzgeber/Fichtner s.u.)

Als systemische Paar- und Familientherapeutin bringe ich das Verständnis und die Methodik mit und mit meinem Traumatherapeutischem Hintergrund die Fähigkeit belastende Erlebnisse der Familienmitglieder und deren Auswirkungen zu erkennen und einzuschätzen.

Grundsätzlich orientiert sich mein fachliches Verständnis an den Qualitätsanforderungen Familien-psychologischer Gutachten in Kindschaftssachen. Sie sind als wissenschaftliche Arbeit zu verstehen und es sind grundlegende Standards einzuhalten (vgl. Zuschlag 2006)

Grundlage sollten anerkannte wissenschaftliche Untersuchungs- und Beurteilungsmethoden sein

Die Nachvollziehbarkeit und die Nachprüfbarkeit, sowohl der Informations- und Datenquellen, der wissenschaftlichen Bezüge wie auch Schlussfolgerungen für alle Verfahrensbeteiligten stellen grundlegende Qualitätsmerkmale dar.

Mit den §§ 156 und 163.2 FamFG wurde die Grundlage gegeben, in aussichtsreichen Fällen ein Hinwirken auf Einvernehmen zu beauftragen. Systemische Beratungsmethoden, angewandt in Einzel-, Eltern- und Mehrpersonensettigs, stellen dann die Methode der Wahl dar.

Hierbei orientiere ich mich an den Standards des Fachverbandes Systemisch-Lösungsorientierter Sachverständiger im Familienrecht (FSLS). Dieses beinhaltet, bereits im Erstgespräch mit den Eltern Einigungsmöglichkeiten abzuklären und Konsequenzen für das Kindeswohl zu verdeutlichen, nach Möglichkeit baldige (moderierte) Einigungsgespräche anzuregen, sowie die Akzeptanz für die Elternrolle des anderen Elternteils zu stärken.

Ausgangspunkt ist in jedem Fall eine enge Orientierung an der Fragestellung des Gerichts (Beweisbeschluss), die gegebenenfalls zeitnah aufgrund erster Eindrücke in Rücksprache mit dem Gericht zu optimieren ist.

In erster Linie sehe ich eine erfolgreiche Arbeit in Fragen zur elterlichen Sorge(Lebensmittelpunkt) und Umgangsrecht.

Das lösungsorientierte Vorgehen zeigt sich aber auch in Fragen möglicher Kindeswohlgefährdungen und elterlicher Erziehungsfähigkeit bzw. Hilfebedarf als sinnvoll: Wenn hierbei nicht nur eine Einschätzung zu diesen Fragen abgeben wird, sondern gleichzeitig am Verständnis der Situation des gefährdeten Kindes und der Zustimmung der Beteiligten mit dem vorgeschlagenen Vorgehen gearbeitet wird. Die Aufgabe des Sachverständigen besteht dabei auch darin, die Kooperation zwischen Jugendamt, helfenden und unterstützenden Institutionen und Eltern zu verbessern oder die Bereitschaft zur Annahme von Hilfen zu erhöhen, was eine Intervention des Gerichts abwenden oder abschwächen hilft.

(Salzgeber/Fichtner: Lösungsorientierte Begutachtung. Auf: http://www.muenchener-Anwaltverein.de/anwaltsportal/kooperationen/muenchner-modell/kolumne/)

Ich hoffe, dass ich Ihnen meinen neuen Tätigkeitsbereich mit meinem fachlichen Verständnis ausreichend veranschaulichen konnte und freue mich auf eine weitere gute Zusammenarbeit im neuen Jahr.

Selbstverständlich führe ich meine Tätigkeit als Verfahrensbeistand und Umgangspflegerin weiter, sofern von Ihnen gewünscht.
