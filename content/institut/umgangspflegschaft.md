---
title: 'Umgangspflegschaft'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: false
featured: true
weight: 3
---


Der Wirkungskreis  der Umgangspflegschaft umfasst die Förderung des Umgangs zwischen dem Kind und dem umgangsberechtigten Elternteil/der Bezugsperson bzw. die Umsetzung einer gerichtlichen Umgangsregelung oder einer entsprechenden Vereinbarung der Eltern.

Ziel jeder Umgangspflegschaft ist es, die beteiligten Erwachsenen (wieder) in die Lage zu versetzen, den Umgang selbständig und zum Wohl des Kindes zu regeln. In vielen Fällen wird echte Einvernehmlichkeit nicht herzustellen sein. Allerdings kann eine Entlastung der Kinder auch möglich sein, wenn es gelingt „friedliche Koexistenz“ der Eltern zu erreichen, d.h. parallele Elternschaft mit einem Mindestmaß an Bindungstoleranz.

Die Umgangspflegschaft hat ferner das Ziel, den Umgang (wieder) als etwas Normales, Selbstverständliches in den Alltag der Beteiligten zu implementieren.

Sie können meinen Einsatz als Umgangspflegerin zwar anregen, aber die Entscheidung und eigentliche Bestellung erfolgt nur durch die zuständige Richterin, bzw. den Richter.

## Gesetzliche Befugnisse
Die Umgangspflegschaft umfasst das Recht, die Herausgabe des Kindes zur Durchführung des Umgangs zu verlangen und für die Dauer des Umgangs dessen Aufenthalt zu bestimmen (§ 1684 Abs. 3 S. 4 BGB).

Das Recht der Eltern ist in soweit gem. §1630 BGB eingeschränkt. Zwangsmittel stehen dem Umgangspfleger jedoch nicht zur Verfügung; Ordnungsmittel oder unmittelbarer Zwang können nur durch das Gericht angeordnet werden (nicht gegen das Kind). Ein gewisser Druck ist nur durch seine Anwesenheit und im Zusammenwirken mit dem Gericht möglich (Berichtspflicht).
