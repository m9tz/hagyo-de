---
title: 'Verfahrensbeistand'
date: 2018-11-18T12:33:46+10:00
txt_bg_image: images/zeichnung1.svg
site_image: images/zeichnung1.svg
intro_image: images/undraw_selection_92i4.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
draft: false
featured: false
weight: 3
---


Meine Aufgabe ist es, das Interesse des Kindes festzustellen und im gerichtlichen Verfahren zur Geltung zu bringen. Ich werde sozusagen zum "Sprachrohr" des Kindes und sensibilisiere die beteiligten Erwachsenen für die Kindperspektive. Gleichzeitig vermittele ich dem Kind Gegenstand, Ablauf und möglichen Ausgang des Verfahrens aus der Erwachsenenperspektive in eine altersgerechte Kindperspektive. Ich kann im Interesse des Kindes Rechtsmittel einlegen, ohne gesetzlicher Vertreter des Kindes zu sein.

Dieser Teil meiner Arbeit steht  im Bezug zu § 158 FamFG (Gesetz über das Verfahren in Familiensachen und in den Angelegenheiten der freiwilligen Gerichtsbarkeit). Nach diesen Gesetzen bestellt das Gericht dem minderjährigen Kind in Kindschaftssachen, die seine Person betreffen, einen Verfahrensbeistand, soweit dies zur Wahrnehmung seiner Interessen erforderlich ist.

Neu ist die Regelung, dass ich vom Gericht in meiner Funktion als Verfahrensbeistand zusätzlich beauftragt werden kann, am Zustandekommen einer einvernehmlichen Regelung (z.B. durch Mediation) auf Erwachsenenseite mitzuwirken.

Sie können meinen Einsatz als Verfahrensbeistand zwar anregen aber _die Entscheidung und eigentliche Bestellung erfolgt nur durch die zuständige Richterin, bzw. den Richter._
