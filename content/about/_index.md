---
title: 'Vita'
menu: 'main'
intro_image: images/undraw/undraw_yoga_248n.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
draft: 'false'
---


# Über mich

geb. 1962, verheiratet, zwei Kinder und zwei Enkelkinder

### Ausbildung als:

* Krankenschwester und Diplom Sozialpädagogin <br>

* Systemische Paar- und Familientherapeutin (EZI Berlin und Zertifikat ZEPRA an der HfAW Hamburg, Prof. Hantel-Quitmann)
<br>

*   Multifamilientherapeutin (MFT Praxis Asen/Scholz)
<br>

*   Traumapädagogik und Traumazentrierte Fachberatung (Zertifikat Zentrum für Psychotraumatologie Kasse/IfP HH, DeGPT/BAG-TP)
<br>

*   PITT Kids - Psychodynamisch Imaginative Traumatherapie für Kinder und Jugendliche (IPKJ - Institut für Psychotraumatologie im Kindes und Jugendalter, Dr. Andreas Krüger)
<br>

*   Ego - State - Therapie (IFHE Institut für klinische Hypnose und Ego - State (Berlin, Kai Fritzsche)
<br>

*   EMDR (Zertifikat Institut für Psychotraumatologie Hamburg und VDH)
<br>

*   Erziehungstrainerin (Triple P)
<br>

*   Trainerin für den Elternkurs für Trennungseltern "Kinder im Blick" (Zertifikat LMU und Familiennotruf München)

### Erfahrungen:
<br>
*   langjährige Erfahrung in Beratung und Therapie in schwierigen Familienkonstellationen
( AFT-aufsuchende Familientherapie, Familienhilfe, Erziehungsberatung in der Familie) allg. Sozialdienst Jugendamt, freien Jugendhilfeträgern und aktuell tätig im Familienberatungteam des Jugendamt Kreis Storman
<br>

*   Erfahrung in Beratung bei innerfamiliärer Gewalt oder sexueller Gewalt (Kinderschutzbund)
<br>

*   großes Erfahrungsspektrum in der Beratung von Paaren/Eltern in Trennungssituationen durch meine langjährige Tätigkeit für Familiengerichte
<br>

*   freiberuflich als Psychotraumaberaterin und -Therapeutin für Erwachsene, Kinder und Jugendliche, syst. Paar- und Familientherapeutin in eigener Praxis und im Auftrag für das Kinder- und Jugendhaus St. Josef, Bad Oldesloe und für das Jugendamt Norderstedt tätig
<br>

*   Im Auftrag verschiedener Familiengericht seit als berufsmäßige Verfahrensbeiständin (Anwalt des Kindes) /Umgangspflegerin/Ergänzungspflegerin, lösungsorientierte Sachverständige tätig
