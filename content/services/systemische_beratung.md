---
title: 'Systemische Therapie'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: false
featured: true
weight: 1
---

Im Unterschied zu vielen anderen Therapieformen betrachtet man in der systemischen Arbeit die geschilderten Probleme des Patienten als ein Gefüge von vielen Teilen. Man spricht von einem System. Ein System stellt z.B. die Familie oder die Arbeitswelt dar. Berät man einen Menschen, so wirken sich die eingesetzten Interventionen nicht nur auf die Person selbst aus, sondern nehmen auch Einfluss auf das System, in dem sich diese Person bewegt, also auf seine Familie oder seine Berufssituation.


Die Beratung von Personen in Arbeitssystemen ist unter dem Begriff Coaching bekannt. Gezielt eingesetzte Interventionen (z.B. Genogrammarbeit, Skulpturen etc.) dienen der Vergrößerung des Möglichkeitsspektrums.

Die systemische Therapie und Beratung sind lösungs- und nicht problemorientiert. Diese Herangehensweise ermöglicht es, Störungen (in Systemen) zu erkennen und sie als Möglichkeiten und Chancen einer neuen Entwicklung zu nutzen. 

Ein sehr wichtiges Element dieser Therapieform stellt die Ressourcenhebung der Person (des Systems) dar. Gelingt es in der Therapie diese Ressourcen zu aktivieren und zu nutzen, so ist das zu erreichende Ziel sehr nah.
