---
title: 'Paartherapie'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: false
featured: true
weight: 3
---

Die Paartherapie hilft Ihnen dabei, festgefahrene Kommunikationsmuster zu unterbrechen.
Ich unterstütze Sie in Dialog zu kommen und den Teufelskreis aus Streit, Distanz oder Sprachlosigkeit zu beenden. Beide Partner haben die Möglichkeit, ihre eigene Sichtweise darzustellen. Gemeinsam werden alte Verhaltensweisen und Verletzungen bearbeitet. Alte Wunden können heilen.
