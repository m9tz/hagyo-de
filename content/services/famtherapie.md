---
title: 'Familientherapie'
date: 2018-11-28T15:14:39+10:00
# image: '/static/img/Hakomi-Logo.png'
featured: true
draft: false
weight: 2
---


Eine Familie muss immer wieder kleine und auch
größere Krisen managen. In der Familientherapie werden Probleme nicht als Eigenschaften einzelner Personen gesehen. Sie sind Ausdruck der aktuellen Kommunikations- und Beziehungsbedingungen in der Familie.

Ziel der Therapie ist eine Erweiterung der Wahrnehmungs- und Handlungsmöglichkeiten des/der Einzelnen und des Gesamtfamiliensystems. Jedes Familienmitglied hat die Chance, seine eigene Sichtweise zu schildern. Mit der Bearbeitung des Konfliktes kann wieder ein liebevoller, konstruktiver Umgang miteinander entstehen.

Durch Gespräche, Familienaufstellungen, Skulpturarbeit und kreative Methoden helfe ich Ihnen, gestärkt aus der Krise zu gehen.
