---
title: 'Psychotraumaberatung'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: false
featured: true
weight: 4
---


Ich biete Unterstützung und Hilfe bei der Distanzierung von traumatischen Erlebnisinhalten, der Stabilisierung der Psyche nach einem traumatischen Erlebnis sowie der Aufklärung  über Zusammenhänge und Verlauf eines Psychotraumas. Auf Wunsch begleite ich Sie durch die schwere Zeit nach dem Trauma.
Ausbildung: Stabilisierungstechniken, Entspannungstechniken, Achtsamkeitsübungen
