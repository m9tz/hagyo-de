---
title: 'Ego State Therapie'
date: 2018-11-28T15:14:39+10:00
# image: '/static/img/Hakomi-Logo.png'
featured: true
draft: false
weight: 6
---


nach Dr. John und Dr. Helen Watkins (USA)

Die Ego-State-Therapie ist ein psychotherapeutischer Modell, der mit Persönlichkeitsanteilen, den sogenannten Ego-States arbeitet und somit auf einem Teilemodell der Persönlichkeit beruht. Sie kann bei einer großen Zahl von Problemen angewendet werden, wie Angst, Depressionen, posttraumatische Belastungsstörungen, Sexual-, Ess- oder Persönlichkeitsstörungen, Abhängigkeitserkrankungen und vieles mehr.

Die Ego-State-Therapie ist eine psychotherapeutische Methode, die dabei helfen kann, innere und äußere Mauern zu überwinden und Heilungsprozesse zu fördern. Sie gleicht in vieler Hinsicht einer Expedition und führt uns in eine unbekannte Landschaft. (Kai Fritzsche, “Praxis der Ego-State-Therapie“ 2013 Carl-Auer-Verlag)

Das Modell geht davon aus, dass wir aus einer Familie von Ego-States bestehen. Wenn wir sagen:

„Ich fühle mich hin- und hergerissen“ oder
„Ich fühle mich wie ausgetauscht.“
„Das passt gar nicht zu mir“
„ich fühle mich (nicht) im Reinen mit mir“
„In diesem Zustand kenne ich gar nicht“
„ein Teil von mir will das, der andere das“,
dann meinen wir einen Ego-State.

Das Modell beruht auf der Annahme, dass sich die Persönlichkeit aus mehreren Anteilen zusammensetzt, also ein multidimensionales Selbst im Gegensatz zur Vorstellung eines konsistenten „Ich“. Es gibt viele Therapieschulen, die von einer inneren Vielfalt ausgehen. Schulz von Thun (Kommunikations- und Beratungspsychologie) z.B. entwickelte das Modell des „Inneren Teams“.

Alltagsanteile (Ego States) kennen wir alle, wie….

Der Träumer – „mhhhh…“

Der Schüchterne – „Ich weiß garnicht…“

Der Faullenzer – „Das hat noch Zeit“.

Der Denker – „Ich habe eine Idee!“

Der Macher – „Los jetzt!“

Das innere Kind – „Ich will aber…“

Der Kontrolleur – „Hey Achtung!“

Der Zweifler – „Ich weiß gar nicht…“

Der innere Kritiker – „Du bist einfach zu blöd…“

In der Ego-State-Therapie geht es in der Regel um zwei Arten von Ich-Anteilen, den ressourcenreichen, die wir in unserem Alltag nutzen können und die dysfunktionalen, reaktiven und nicht mit der gegenwärtigen Realität verbundenen Ich-Anteilen(Robin Shapiro „Ego-State-Interventionen“ Probst Verlag 2019).

Ziel ist es diese bewusst zu machen, die ressourcenreichen zu stärken und beide Gruppen miteinander in Kontakt zu bringen und zu integrieren. Dabei geht es gleichermaßen um innere Klärung und die Erhöhung der eigenen Bewältigungsfähigkeit.

Die Ego-State-Therapie ist eine integrative Therapie, die problemlos mit anderen Therapieformen verbunden werden kann.

Die Ego-State-Therapie ist sehr wirksam und oft kann man schon nach wenigen Sitzungen positive Veränderungen spüren.
