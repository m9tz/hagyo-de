---
title: 'PITT Kid'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: false
featured: true
weight: 7
---

PITT Kid - Psychodynamisch Imaginative Traumatherapie für Kinder und Jugendliche

nach Luise Reedemann und Dr.Andreas Krüger

Durch die psychodynamisch imaginative Traumatherapie für Kinder und Jugendliche (PITT Kids) lernen Kinder und Jugendlichen mit den Symptomen (Störungszeichen) einer Belastungs-/Trauma Störung umzugehen. Sie lernen, diese, zusammen mit den Bezugspersonen, als „Notlösungsversuch“ der eigenen Psyche zu verstehen und so zunächst den Respekt vor sich selbst wieder zu gewinnen.
Sie lernen durch das Nutzen eigener Kraftquellen und der Arbeit mit heilsamen Vorstellungen Impulse und Gefühle besser zu regulieren, sich von überwältigenden Zuständen zu distanzieren, innerlich Abstand zu traumatischen Erinnerungen usw. aufzubauen. Eine besondere Bedeutung kommt der Arbeit mit beschädigten so genannten „Ego-States“, den „Inneren jüngeren Kindern“ zu. Diese entsprechen schwer verletzen Anteilen des „Ich“ aus vergangenen Zeiten der traumatischen Erfahrungen, die auch heute noch das Leben (mit) bestimmen.
Die Stabilisierung der Persönlichkeit, die Distanzierung von traumatischen Erinnerungen und die Arbeit mit verletzen Anteilen steht im Mittelpunkt der Arbeit.


## Wichtige Grundsätze dabei sind:
* Die Berücksichtigung aller Entwicklungsphasen des jungen Menschen
* Die Betonung altersspezifischer Ressourcen (Stärken)
* Die Einbeziehung der Familie und/oder des sozialen Umfelds des Kindes.

## Aufgaben und Ziele
* Minderung und Behebung seelischer Leidenszustände sowie körperlicher
   Beeinträchtigungen
* Förderung der Persönlichkeitsentwicklung
* Abbau von Verhaltensstörungen und Problemen, die die familiäre, soziale,
   schulische und berufliche Integration behindern
* Hilfe bei der Verarbeitung belastender und überfordernder Situationen
* Entwicklung realitätsgerechter Handlungsperspektiven
