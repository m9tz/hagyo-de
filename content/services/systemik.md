---
title: 'Systemische Therapie'
date: 2018-11-18T12:33:46+10:00
image: '/services/noun_591323.png'
draft: true
featured: true
weight: 3
---


oder Systemische Familientherapie ist ein psychotherapeutisches Verfahren, dessen Schwerpunkt auf dem sozialen Kontext psychischer Störungen, insbesondere auf Interaktionen zwischen Mitgliedern der Familie und deren sozialer Umwelt liegt. In Abgrenzung zur Psychoanalyse betonen Vertreter dieser Therapierichtung die Bedeutung impliziter Normen des Zusammenlebens für das Zustandekommen und die Überwindung psychischer Störungen (Familienregeln).

Allerdings berücksichtigen auch andere Therapieformen wie zum Beispiel die Kognitive Kurzzeittherapie den ‘systemischen’ Aspekt. Die Systemische Therapie unterscheidet sich nach Angaben ihrer Vertreter dadurch, dass weitere Mitglieder des für den Patienten relevanten sozialen Umfeldes in die Behandlung einbezogen werden.

Seit Ende 2008 wird in Deutschland die Systemische Therapie als wissenschaftliches Psychotherapieverfahren anerkannt.
