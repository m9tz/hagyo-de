---
title: 'EMDR'
description: ''
date: 2018-11-28T15:14:39+10:00
image: '/static/images/Hakomi-Logo.png'
featured: true
draft: false
intro_image: images/undraw_selection_92i4.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
weight: 5
---



EMDR steht für – Eye Movement Desensitization and Reprocessing –

(Desensibilisierung und Verarbeitung durch Augenbewegung)

EMDR wurde um 1990 durch Francine Shapiro, eine amerikanische Psychotherapeutin, entdeckt, ausgearbeitet und an traumatisierten Patienten erprobt. Heute wird die Methode weltweit in der eingesetzt und man kann sie neben der Traumabearbeitung auch für viele andere Störungen zur Bewältigung einsetzen.

Stressregulierung
Angstregulierung (Ängste/Phobien/Panikattacken)
Veränderung dysfunktionaler Überzeugungen und Verhalten
Entwicklung eigener Lösungsstrategien
Coaching (Höchstleistungen, Prüfungsangst, Lampenfieber)
Depression
Sucht
Essstörungen
Anpassungsstörungen
Krankheitsbewältigung
Zunächst werden die schwierigen Erlebnisse zu erkunden, z.B. was man über sich selbst in der Situation denkt und wie sich das im Körper anfühlt. Denn meist sind dies prägende einschränkende Reaktionsmuster. Ebenfalls sehr wichtig ist die Zukunftsvision. Im weiteren Ablauf wird schon einmal erkundet, wo es hingehen soll, welche neue Einstellung die alte ablösen soll.

Dann werden mithilfe gezielter Augenbewegungen oder taktiler Reize traumatische Erinnerungen entlastet, Ressourcen erschlossen und Leistungsstress reguliert.

Das Gehirn wird angeregt, diese Erfahrung neu zu verarbeiten und im Bewusstsein der Bewältigungsmöglichkeiten des Erwachsenen neu zu interpretieren

Es ist die Methode der Psychotherapie, die am häufigsten überprüft und ausgewertet wurde. Durch bildgebende Verfahren wie PET weiß man, dass dadurch die Hirnströme verändert werden.
