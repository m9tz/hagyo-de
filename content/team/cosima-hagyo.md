---
title: 'Cosima Hagyó'
date: 2020-01-19
draft: false
image: 'images/team/cosima.jpg'
jobtitle: 'Dipl. Sozial Pädagogin & Therapeutin'
email: 'cosima@hagyo.de'
wwwurl: 'https://www.hagyo.de'
weight: 2
---

Syst. Paar- und Familientherapeutin

* Traumatherapie PITT Kids
* EgoState/EMDR/Hypnotherapie
* Verfahrensbeistand/Umgangs-und Ergänzungspflegschaft/Vormund
* Lösungsorientierte Sachverständige
