---
title: 'Team'
menu: main
weight: 2
intro_image: images/undraw/undraw_friends_online_klj6.svg
intro_image_absolute: false
intro_image_hide_on_mobile: false
---

# Das Team und ich

Ich hoffe Sie können durch diese Seiten und die Links auf andere Profile von mir eine Eindruck bekommen. 

Paar- und Familientherapie biete ich gerne auch mit Co-Therapeuten an.
